#!/bin/bash
#. docker*sh
cd ./ghost-cms
#https://stackoverflow.com/questions/255414/why-doesnt-cd-work-in-a-shell-script
#alias proj="cd /home/tree/projects/java"
#. proj

#https://openclassrooms.com/fr/courses/2035766-optimisez-votre-deploiement-en-creant-des-conteneurs-avec-docker/6211517-creez-votre-premier-dockerfile
#sudo dockerd 
sudo docker build -t dockerfile-moi-p3c3img  .

#/ ou : 
sudo docker tag dockerfile-moi-p3c3img moueza/dockerfile-moi-p3c3-dockerhub
sudo apt install gnupg2 pass
#rm ~/.docker -R
sudo docker login
sudo docker push moueza/dockerfile-moi-p3c3-dockerhub

cd -
